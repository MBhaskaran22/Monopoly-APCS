
public interface IPiece
{
    String getName();

    void setOwner(String owner);
}
