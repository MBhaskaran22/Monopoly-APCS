import java.util.ArrayList;
import java.util.Scanner;

public class Monopoly
{
    public static Dice Dice;
    public static Square[] Board = new Square[28];
    public static String Player1;
    public static String Player2;
    public static float Player1Money = 0;
    public static float Player2Money = 0;
    public static int Player1SkippedTurns = 0;
    public static int Player2SkippedTurns = 0;
    public static int Player1Turns = 0;
    public static int Player2Turns = 0;
    public static int Player1Index;
    public static int Player2Index;
    public static int Cycles = 0;
    public static Piece Player1P;
    public static Piece Player2P;
    public static Scanner input;

    public static void main(String[] args)
    {
        Dice = new Dice();

        //populate board
        Board[0] = new Square("Mowry Blvd.", "Property",100,true);
        Board[1] = new Square("Yellow Road", "Property", 150, true);
        Board[2] = new Square("Chance", "Chance", 0, false);
        Board[3] = new Square("Railroad 1", "Property",200, true);
        Board[4] = new Square("China Store", "Property", 150, true);
        Board[5] = new Square("Unnamed Hotel", "Property",150, true);
        Board[6] = new Square("Jail", "Jail", 0, false);
        Board[7] = new Square("Grocery Store","Property", 200, true);
        Board[8] = new Square("Fry's Electronics", "Property", 350, true);
        Board[9] = new Square("Railroad 2", "Property", 200, true);
        Board[10] = new Square("Chance", "Chance", 0, false);
        Board[11] = new Square("Tech Store", "Property", 250, true);
        Board[12] = new Square("School", "Property", 300, true);
        Board[13] = new Square("Free Parking", "Free Parking", 0, false);
        Board[14] = new Square("The Mall", "Property", 300, true);
        Board[15] = new Square("Cinema", "Property", 350, true);
        Board[16] = new Square("Railroad 3", "Property", 200, true);
        Board[17] = new Square("Chance", "Chance", 0, false);
        Board[18] = new Square("Utility", "Property", 200, true);
        Board[19] = new Square("Luxury Tax","Lux", 250, false);
        Board[20] = new Square("Jail", "Jail", 0, false);
        Board[21] = new Square("Shoe Store", "Property", 390, true);
        Board[22] = new Square("Random Store", "Property", 400, true);
        Board[23] = new Square("Chance", "Chance", 0, false);
        Board[24] = new Square("Railroad 4", "Property", 200, false);
        Board[25] = new Square("Disney World", "Property", 600, true);
        Board[26] = new Square("Disney Land", "Property", 450, true);
        Board[27] = new Square("GO!", "Property", 0, false);

        input = new Scanner(System.in);

        while(true)
        {
            System.out.println("Player 1, please enter your piece: (Valid options: Shoe, Car, Nuke, Donald Trump, Mr. Jan)");
            String piece1 = input.next();
            if(piece1.equals("Shoe")) {
                Player1 = "Sho";
                System.out.println("You will be a shoe!");
                break;
            }
            else if(piece1.equals("Car")) {
                Player1 = "Car";
                System.out.println("You will be a car!");
                break;
            }
            else if(piece1.equals("Nuke")) {
                Player1 = "Nuk";
                System.out.println("You will be a nuke!");
                break;
            }
            else if(piece1.equals("Donald Trump")) {
                Player1 = "DT ";
                System.out.println("You will be Donald Trump!");
                break;
            }
            else if(piece1.equals("Mr. Jan")) {
                Player1 = "MrJ";
                System.out.println("You will be Mr. Jan!");
                break;
            }
            else
            {
                System.out.println("That is an invalid option!");
            }
        }

        while(true)
        {
            System.out.println("Player 2, please enter your piece: (Valid options: Shoe, Car, Nuke, Donald Trump, Mr. Jan)");
            String piece1 = input.next();
            if(piece1.equals("Shoe")) {
                if(Player1.equals("Sho"))
                {
                    System.out.println("Player 1 has already taken that option!");
                }
                else {
                    Player2 = "Sho";
                    System.out.println("You will be a shoe!");
                    break;
                }
            }
            else if(piece1.equals("Car")) {
                if(Player1.equals("Car"))
                {
                    System.out.println("Player 1 has already taken that option!");
                }
                else {
                    Player2 = "Car";
                    System.out.println("You will be a car");
                    break;
                }
            }
            else if(piece1.equals("Nuke")) {
                if(Player1.equals("Nuk"))
                {
                    System.out.println("Player 1 has already taken that option!");
                }
                else {
                    Player2 = "Nuk";
                    System.out.println("You will be a nuke!");
                    break;
                }
            }
            else if(piece1.equals("Donald Trump")) {
                if(Player1.equals("DT "))
                {
                    System.out.println("Player 1 has already taken that option!");
                }
                else {
                    Player2 = "DT ";
                    System.out.println("You will be Donald Trump!");
                    break;
                }
            }
            else if(piece1.equals("Mr. Jan")) {
                if(Player1.equals("MrJ")){
                    System.out.println("Player 1 has already taken that option!");
                }
                else {
                    Player2 = "MrJ";
                    System.out.println("You will be Mr. Jan!");
                    break;
                }
            }
            else
            {
                System.out.println("That is an invalid option!");
            }

        }

        Player1P = new Piece();
        Player1P.setOwner(Player1);
        Player1P.num = 1;
        Player2P = new Piece();
        Player2P.setOwner(Player2);
        Player2P.num = 2;

        Board[27].setPiece1(Player1P);
        Board[27].setPiece2(Player2P);
        Player1P.setIndex(27);
        Player2P.setIndex(27);
        Player1P.Money = 1000;
        Player2P.Money = 1000;

        System.out.println("The game will now begin...");
        //blah too busy rn to continue
        while(true)
        {
            Turn(Player1P, 1);
            CBoard.PrintBoard();

            if(Player1P.Money <= 0){
                System.out.println("Player 2 wins!");
                break;
            }else if(Player2P.Money <= 0){
                System.out.println("Player 1 wins!");
                break;
            }

            Turn(Player2P, 2);
            CBoard.PrintBoard();

            if(Player1P.Money <= 0){
                System.out.println("Player 2 wins!");
                break;
            }else if(Player2P.Money <= 0){
                System.out.println("Player 1 wins!");
                break;
            }
        }
    }


    public static void Turn(Piece pc, int ind)
    {
        //player 2
        if(pc.getSkippedTurns() <= 0) {
            System.out.println("Player "+ind+", it is your turn.");
            if (!pc.InJail) {
                //Prompt for dice roll
                System.out.println("Player "+ind+", press any key to roll the dice.");
                String s = input.next();
                int roll = Dice.nextRoll();
                //show roll
                System.out.println("You rolled a " + roll);
                //perform move
                pc.movePiece(roll);
                //say hi
                System.out.println("Welcome to " + pc.getSquare().getName());

                if (pc.getSquare().IsOwnable()) {
                    //If its the jail
                    if (pc.getSquare().getTileType().equals("Jail")) {
                        System.out.println("You're now in jail!");
                    }

                    //If it's not owned
                    if (!pc.getSquare().isOwned()) {
                        //Ask to buy
                        System.out.println("Would you like to buy " + pc.getSquare().getName() + " for " + pc.getSquare().getPrice() + "? (y/n)");
                        //y/n
                        if (input.next().toLowerCase().equals("y")) {
                            pc.getSquare().purchase(ind);
                        }
                    }
                }

                if(pc.getSquare().getTileType().equals("Chance")){
                    System.out.println("You landed on a chance spot, press any key to draw a chance card!");
                    String sggg = input.next();
                    pc.pickChance();
                }
            }
            else{
                System.out.println("Player " + ind + " you are currently in jail!");
                if(pc.hasChance(Chance.GetOutOfJail)){
                    System.out.println("You have a get out of jail free card, would you like to use it? (y/n)");
                    String resp = input.next();
                    if(resp.toLowerCase().equals("y")){
                        pc.useChance(Chance.GetOutOfJail);
                        pc.InJail = false;
                        System.out.println("You are now free!");
                    }
                }
                if(pc.InJail) {
                    System.out.println("You must roll a double to get out. Press any key to do this.");
                    int roll1 = Dice.nextRoll();
                    int roll2 = Dice.nextRoll();
                    System.out.println("You rolled " + roll1 + " and a " + roll2);
                    if(roll1 != roll2){
                        System.out.println("This is not a double!\n Good luck next time!");
                    }else{
                        System.out.println("This is a double!\n You're now free!");
                        pc.InJail = false;
                    }
                }
            }
            pc.handleCharge();
            System.out.println("Player "+ind+", you have $" + pc.Money);

            pc.addTurn();
        }else{
            pc.SkippedTurns--;
        }
    }
}