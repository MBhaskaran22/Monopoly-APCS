import java.util.*;
import java.time.*;

//Dice class
public class Dice
{
    Random random;

    Dice()
    {
        //Clock object
        Clock clock = Clock.systemUTC();
        random = new Random();
        //need to seed the psudo-random generator
        random.setSeed(clock.millis());
    }

    public int nextRoll()
    {
        return random.nextInt(6) + 1;
    }
}
